group: bitrium.gongen.tools
name: genconv
version: 1.0
root: Config
classes:
  GlyphRange:
    start: u32 / range start
    end: u32 / range end
  Font:
    input: str / input .ttf/.otf file
    output: str / output .genfont
    bitmap_size: u8 = 32 / bitmap size
    bitmap_margin: u8 = 4 / bitmap margin(in pixels)
    msdf_range: f64 / msdf range
    msdf_max_angle: f64 = 3 / msdf max angle(in radians)
    ranges: lst[GlyphRange] / unicode codepoint ranges to generate
  Meshes:
    path: str / path to mesh file/folder with mesh files
    multiple_files: u8 = 0 / if set to 1 all files from path are processed
    out: str / output path/folder
    animated: u8 = 0 / if files have animation
  Config:
    fonts: lst[Font] / list of fonts
    meshes: lst[Meshes] / list of meshes
