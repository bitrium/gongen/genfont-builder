#include "conv.hpp"
#include <gongen/gsf/data.hpp>
#include <gongen/common/platform/system.hpp>
#include <gongen/common/compression.hpp>
#include <gongen/common/platform/system.hpp>
#include <assimp/Importer.hpp>
#include <assimp/postprocess.h>
#include <assimp/scene.h>
#include <meshoptimizer.h>
#include "genmesh.hpp"

using namespace gen::gsf;

void process_mesh(const gen::String& config_path) {
	Assimp::Importer importer;

	Schema schema = std::move(Schema::txt_read(new gen::File("data/config.gts")));
	Article doc = std::move(Article::txt_read(schema, new gen::File(config_path.c_str())));
	auto root = doc.access_root();
	auto meshes = root.decode_object().access_at_key("meshes").get().decode_list();
	for (size_t f = 0; f < meshes.element_count(); f++) {
		auto mesh = meshes.get_element(f).get().decode_object();
		
		gen::DynamicArray<gen::Path> files;
		gen::String output = mesh.access_at_key("out").get().decode_str();
		gen::String path = mesh.access_at_key("path").get().decode_str();
		uint8_t multiple_files = mesh.access_at_key("multiple_files").get().decode_u8();
		uint8_t animated = mesh.access_at_key("animated").get().decode_u8();

		if (multiple_files) {
			auto paths = gen::file::Path(path).enumerate<gen::Path>();
			for (auto& p : paths) {
				files.add(p);
			}
		} else files.add(gen::Path(path));

		for (auto& path : files) {
			gen::ByteBuffer data;
			gmesh::Header header;

			const aiScene* scene = importer.ReadFile(path.c_str(), aiProcess_Triangulate | aiProcess_LimitBoneWeights | aiProcess_RemoveRedundantMaterials | aiProcess_TransformUVCoords | aiProcess_OptimizeMeshes | aiProcess_OptimizeGraph);
			if (!scene) {
				throw gen::RuntimeException(importer.GetErrorString());
			}

			printf("Loaded %s\n", path.c_str());

			header.mat_count = scene->mNumMaterials;
			header.mat_offset = data.size();
			for (size_t i = 0; i < scene->mNumMaterials; i++) {
				const aiMaterial* mat = scene->mMaterials[i];

				aiColor4D diffuse_color;
				float roughness, metallic;

				if (aiGetMaterialColor(mat, AI_MATKEY_COLOR_DIFFUSE, &diffuse_color) != AI_SUCCESS) diffuse_color = aiColor4D(1.0f, 1.0f, 1.0f, 1.0f);
				if (aiGetMaterialFloat(mat, AI_MATKEY_ROUGHNESS_FACTOR, &roughness)) roughness = 1.0f;
				if (aiGetMaterialFloat(mat, AI_MATKEY_METALLIC_FACTOR, &metallic)) metallic = 0.0f;

				aiString name = mat->GetName();
				gmesh::Material* gmesh_mat = (gmesh::Material*)data.reserve(sizeof(gmesh::Material) + name.length);
				memcpy(gmesh_mat->diffiuse_color, &diffuse_color.r, sizeof(float) * 4);
				gmesh_mat->roughness = roughness;
				gmesh_mat->metalic = metallic;
				gmesh_mat->name_len = name.length;
				memcpy(gmesh_mat->name, name.data, name.length);
			}
			printf("Mesh 1/3\n");
			if (animated) {
				gen::Buffer<aiString> bone_names;

				header.flags |= gmesh::FLAG_ANIMATED;
				header.bone_offset = data.size();
				for (size_t i = 0; i < scene->mNumMeshes; i++) {
					const aiMesh* mesh = scene->mMeshes[i];
					for (size_t j = 0; j < mesh->mNumBones; j++) {
						const aiBone* bone = mesh->mBones[j];
						aiString name = bone->mName;
						bone_names.add(name);

						gmesh::Bone* gmesh_bone = (gmesh::Bone*)data.reserve(sizeof(gmesh::Bone) + name.length);
						ai_real* mat = (ai_real*)&bone->mOffsetMatrix.a1;
						for (size_t i = 0; i < 16; i++) gmesh_bone->inverse_bind_matrix[i] = mat[i];
						gmesh_bone->name_len = name.length;
						memcpy(gmesh_bone->name, name.data, name.length);

						header.bone_count++;
					}
				}
				printf("Animation 1/2\n");

				header.anim_count = scene->mNumAnimations;
				header.anim_offset = data.size();
				for (size_t i = 0; i < scene->mNumAnimations; i++) {
					const aiAnimation* anim = scene->mAnimations[i];
					aiString name = anim->mName;

					gmesh::Animation* gmesh_animation = (gmesh::Animation*)data.reserve(sizeof(gmesh::Animation) + name.length);
					gmesh_animation->channel_count = 0;
					gmesh_animation->name_len = name.length;
					memcpy(gmesh_animation->name, name.data, name.length);

					gen::Buffer<uint32_t> bone_ids;
					for (size_t j = 0; j < anim->mNumChannels; j++) {
						uint32_t bone_id = 256;
						for (size_t k = 0; k < bone_names.size(); k++) {
							if (strcmp(anim->mChannels[j]->mNodeName.C_Str(), bone_names[k].C_Str()) == 0) {
								bone_id = k;
								gmesh_animation->channel_count++;
								break;
							}
						}
						bone_ids.add(bone_id);
					}

					for (size_t j = 0; j < anim->mNumChannels; j++) {
						const aiNodeAnim* channel = anim->mChannels[j];
						
						if (bone_ids[j] > 255) continue;

						gmesh::Channel* gmesh_channel = (gmesh::Channel*)data.reserve(sizeof(gmesh::Channel));
						gmesh_channel->bone_id = bone_ids[j];
						gmesh_channel->pos_keyframes = channel->mNumPositionKeys;
						gmesh_channel->scale_keyframes = channel->mNumScalingKeys;
						gmesh_channel->rot_keyframes = channel->mNumRotationKeys;
						if (channel->mPreState == aiAnimBehaviour_DEFAULT) gmesh_channel->pre_state = gmesh::CHANNEL_STATE_DEFAULT;
						if (channel->mPreState == aiAnimBehaviour_CONSTANT) gmesh_channel->pre_state = gmesh::CHANNEL_STATE_CONSTANT;
						if (channel->mPreState == aiAnimBehaviour_LINEAR) gmesh_channel->pre_state = gmesh::CHANNEL_STATE_LINEAR;
						if (channel->mPreState == aiAnimBehaviour_REPEAT) gmesh_channel->pre_state = gmesh::CHANNEL_STATE_REPEAT;
						if (channel->mPostState == aiAnimBehaviour_DEFAULT) gmesh_channel->post_state = gmesh::CHANNEL_STATE_DEFAULT;
						if (channel->mPostState == aiAnimBehaviour_CONSTANT) gmesh_channel->post_state = gmesh::CHANNEL_STATE_CONSTANT;
						if (channel->mPostState == aiAnimBehaviour_LINEAR) gmesh_channel->post_state = gmesh::CHANNEL_STATE_LINEAR;
						if (channel->mPostState == aiAnimBehaviour_REPEAT) gmesh_channel->post_state = gmesh::CHANNEL_STATE_REPEAT;

						for (size_t k = 0; k < channel->mNumPositionKeys; k++) {
							gmesh::VectorKeyframe* gmesh_key = (gmesh::VectorKeyframe*)data.reserve(sizeof(gmesh::VectorKeyframe));

							const aiVectorKey key = channel->mPositionKeys[k];
							gmesh_key->time = key.mTime;
							gmesh_key->value[0] = key.mValue.x;
							gmesh_key->value[1] = key.mValue.y;
							gmesh_key->value[2] = key.mValue.z;
							if (key.mInterpolation == aiAnimInterpolation_Step) gmesh_key->interpolation = 0;
							else gmesh_key->interpolation = 1;
						}
						for (size_t k = 0; k < channel->mNumScalingKeys; k++) {
							gmesh::VectorKeyframe* gmesh_key = (gmesh::VectorKeyframe*)data.reserve(sizeof(gmesh::VectorKeyframe));

							const aiVectorKey key = channel->mScalingKeys[k];
							gmesh_key->time = key.mTime;
							gmesh_key->value[0] = key.mValue.x;
							gmesh_key->value[1] = key.mValue.y;
							gmesh_key->value[2] = key.mValue.z;
							if (key.mInterpolation == aiAnimInterpolation_Step) gmesh_key->interpolation = 0;
							else gmesh_key->interpolation = 1;
						}
						for (size_t k = 0; k < channel->mNumRotationKeys; k++) {
							gmesh::QuatKeyframe* gmesh_key = (gmesh::QuatKeyframe*)data.reserve(sizeof(gmesh::QuatKeyframe));

							const aiQuatKey key = channel->mRotationKeys[k];
							gmesh_key->time = key.mTime;
							gmesh_key->value[0] = key.mValue.x;
							gmesh_key->value[1] = key.mValue.y;
							gmesh_key->value[2] = key.mValue.z;
							gmesh_key->value[3] = key.mValue.w;
							if (key.mInterpolation == aiAnimInterpolation_Step) gmesh_key->interpolation = 0;
							else gmesh_key->interpolation = 1;
						}
					}
				}
				printf("Animation 2/2\n");
			}
			
			gen::ByteBuffer vtx_data;
			gen::Buffer<uint32_t> idx_data;
			uint32_t vtx_size = sizeof(gmesh::Vertex);
			if (animated) vtx_size = sizeof(gmesh::AnimatedVertex);

			for (size_t i = 0; i < scene->mNumMeshes; i++) {
				uint32_t idx_offset = vtx_data.size_bytes() / vtx_size;

				const aiMesh* mesh = scene->mMeshes[i];
				uint8_t mat = mesh->mMaterialIndex;

				for (size_t j = 0; j < mesh->mNumVertices; j++) {
					gmesh::Vertex* vtx = (gmesh::Vertex*)vtx_data.reserve(vtx_size);

					vtx->x = mesh->mVertices[j].x;
					vtx->y = mesh->mVertices[j].y;
					vtx->z = mesh->mVertices[j].z;
					vtx->nx = ((mesh->mNormals[j].x * 2.0f) - 1.0f) * 255;
					vtx->ny = ((mesh->mNormals[j].y * 2.0f) - 1.0f) * 255;
					vtx->nz = ((mesh->mNormals[j].z * 2.0f) - 1.0f) * 255;
					vtx->mat = mat;

					if (animated) {
						gmesh::AnimatedVertex* vtx2 = (gmesh::AnimatedVertex*)vtx;
						for (size_t i = 0; i < 4; i++) {
							vtx2->bones[i] = 0;
							vtx2->weights[i] = 0.0f;
						}
					}
				}

				for (size_t j = 0; j < mesh->mNumFaces; j++) {
					aiFace face = mesh->mFaces[j];
					if (face.mNumIndices == 3) {
						idx_data.add(idx_offset + face.mIndices[0]);
						idx_data.add(idx_offset + face.mIndices[1]);
						idx_data.add(idx_offset + face.mIndices[2]);
					}
				}

				if (animated) {
					gen::Buffer<uint8_t> vtx_bone_count;
					for (size_t j = 0; j < mesh->mNumVertices; j++) vtx_bone_count.add(0);

					for (size_t j = 0; j < mesh->mNumBones; j++) {
						const aiBone* bone = mesh->mBones[j];

						for (size_t k = 0; k < bone->mNumWeights; k++) {
							aiVertexWeight weight = bone->mWeights[k];
							gmesh::AnimatedVertex* vtx = (gmesh::AnimatedVertex*)vtx_data.ptr((idx_offset + weight.mVertexId) * vtx_size);
							vtx->bones[vtx_bone_count[weight.mVertexId]] = j;
							vtx->weights[vtx_bone_count[weight.mVertexId]] = weight.mWeight * UINT16_MAX;
							vtx_bone_count[weight.mVertexId]++;
						}
					}
				}
			}
			printf("Mesh 2/3\n");

			gen::Buffer<uint32_t> idx_data2;
			gen::ByteBuffer vtx_data2;
			uint32_t vtx_count = vtx_data.size_bytes() / vtx_size;
			meshopt_optimizeVertexCache(idx_data2.reserve(idx_data.size()), idx_data.ptr(), idx_data.size(), vtx_count);
			meshopt_optimizeOverdraw(idx_data.ptr(), idx_data2.ptr(), idx_data.size(), (float*)vtx_data.ptr(), vtx_count, vtx_size, 1.05f);
			meshopt_optimizeVertexFetch(vtx_data2.reserve(vtx_data.size_bytes()), idx_data.ptr(), idx_data.size(), vtx_data.ptr(), vtx_count, vtx_size);
			printf("Mesh 3/3\n");

			header.vtx_offset = data.size();
			data.add(vtx_data2.ptr(), vtx_data.size_bytes());
			header.idx_offset = data.size();
			data.add(idx_data.ptr(), idx_data.size_bytes());

			gen::String output_path = output;
			if (multiple_files) {
				output_path = (gen::Path(output) / path.stem()).string();
				output_path.append(".gmesh");
			}

			gen::ByteBuffer compressed_data;
			gen::compressor_zlib()->compress(compressed_data, data.ptr(), data.size());

			gen::File file(output_path.c_str(), gen::file::Mode::WRITE);
			file.write(&header, header.header_size);
			file.write(compressed_data.ptr(), compressed_data.size());
			file.flush();
			file.close();

			printf("Saved %s\n", output_path.c_str());
		}
	}
}
