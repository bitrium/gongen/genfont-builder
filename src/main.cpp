#include <gongen/common/platform.hpp>
#include "conv.hpp"

int main(int argc, char** argv) {
	try {
		gen::String config_path = "config.gta";
		if (argc > 1) {
			config_path = gen::String(argv[1]);
		}
		process_mesh(config_path);
		process_font(config_path);
		return 0;
	}
	catch (gen::Exception& e) {
		gen::console::init();
		gen::console::writef("GenConv Failed: %s", e.to_string().c_str());
		return -1;
	}
}
