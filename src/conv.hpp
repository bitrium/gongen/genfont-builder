#pragma once

#include <gongen/common/containers/string.hpp>

void process_font(const gen::String& config_path);
void process_mesh(const gen::String& config_path);
