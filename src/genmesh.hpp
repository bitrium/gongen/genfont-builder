#pragma once

#include <gongen/common/defines.hpp>

namespace gmesh {
const static uint32_t MAGIC = GEN_FOURCC_STR("G\xC7M\r");
const static uint8_t VERSION = 1;

const static uint8_t FLAG_ANIMATED = 1;
const static uint8_t FLAG_ZSTD = 2;

const static uint8_t CHANNEL_STATE_DEFAULT = 0;
const static uint8_t CHANNEL_STATE_CONSTANT = 1;
const static uint8_t CHANNEL_STATE_LINEAR = 2;
const static uint8_t CHANNEL_STATE_REPEAT = 3;

#pragma pack(push, 1)
struct Header {
	uint32_t magic = MAGIC;
	uint8_t version = VERSION;
	uint8_t flags = 0;
	uint8_t header_size = sizeof(Header);
	uint8_t reserved1 = 0;

	uint8_t mat_count = 0;
	uint8_t bone_count = 0;
	uint8_t anim_count = 0;
	uint8_t reserved2 = 0;

	uint32_t mat_offset = 0;
	uint32_t bone_offset = 0;
	uint32_t anim_offset = 0;
	uint32_t vtx_offset = 0;
	uint32_t idx_offset = 0;
};
struct Material {
	float diffiuse_color[4];
	float metalic;
	float roughness;
	uint8_t name_len;
	char name[];
};
struct Bone {
	float inverse_bind_matrix[16];
	uint8_t name_len;
	char name[];
};
struct Animation {
	float duration;
	float tps;
	uint32_t channel_count;
	uint8_t name_len;
	char name[];
};
struct Channel {
	uint8_t bone_id;
	uint32_t pos_keyframes;
	uint32_t scale_keyframes;
	uint32_t rot_keyframes;
	uint8_t pre_state;
	uint8_t post_state;
};
struct VectorKeyframe {
	uint8_t interpolation; //0 - step, 1 - lerp
	float time;
	float value[3];
};
struct QuatKeyframe {
	uint8_t interpolation; //0 - step, 1 - slerp
	float time;
	float value[4];
};
struct Vertex {
	float x, y, z;
	uint8_t nx, ny, nz, mat;
};
struct AnimatedVertex :public Vertex {
	uint8_t bones[4];
	uint16_t weights[4];
};
#pragma pack(pop)
}
