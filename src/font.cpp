#include "conv.hpp"
#include <gongen/gsf/data.hpp>
#include <gongen/common/platform/system.hpp>
#include <gongen/common/compression.hpp>
#include <ft2build.h>
#include FT_FREETYPE_H
#include <gongen/loaders/font.hpp>
#include <msdfgen.h>
#include <msdfgen-ext.h>
#define STB_IMAGE_WRITE_IMPLEMENTATION
#include <stb_image_write.h>

using namespace gen::gsf;
using namespace gen::loaders::internal;

struct Range {
	uint32_t start;
	uint32_t end;
};

void process_font(const gen::String& config_path) {
	FT_Library ft_lib;
	int error = FT_Init_FreeType(&ft_lib);
	if (error) throw gen::RuntimeException::createf("Can't initialize freetype: %s", FT_Error_String(error));

	Schema schema = std::move(Schema::txt_read(new gen::File("data/config.gts")));
	Article doc = std::move(Article::txt_read(schema, new gen::File(config_path.c_str())));
	auto root = doc.access_root();
	auto fonts = root.decode_object().access_at_key("fonts").get().decode_list();
	for (size_t f = 0; f < fonts.element_count(); f++) {
		auto font = fonts.get_element(f).get().decode_object();
		gen::String input = font.access_at_key("input").get().decode_str();
		gen::String output = font.access_at_key("output").get().decode_str();
		double msdf_range = font.access_at_key("msdf_range").get().decode_f64();
		double msdf_max_angle = font.access_at_key("msdf_max_angle").get().decode_f64();
		uint8_t bitmap_size = font.access_at_key("bitmap_size").get().decode_u8();
		uint8_t bitmap_min_size = font.access_at_key("bitmap_min_size").get().decode_u8();
		uint8_t bitmap_margin = font.access_at_key("bitmap_margin").get().decode_u8();
		gen::Buffer<Range> ranges;
		auto ranges_gsf = font.access_at_key("ranges").get().decode_list();
		for (size_t j = 0; j < ranges_gsf.element_count(); j++) {
			auto range = ranges_gsf.get_element(j).get().decode_object();
			Range r;
			r.start = range.access_at_key("start").get().decode_u32();
			r.end = range.access_at_key("end").get().decode_u32();
			ranges.add(r);
		}

		FT_Face face;
		error = FT_New_Face(ft_lib, input.c_str(), 0, &face);
		if(error) throw gen::RuntimeException::createf("Can't load font %s: %s", input.c_str(), FT_Error_String(error));
		FT_Select_Charmap(face, FT_ENCODING_UNICODE);
		FT_Set_Pixel_Sizes(face, 255, 255);

		printf("Loaded %s\n", input.c_str());

		gen::File out_file(output.c_str(), gen::file::Mode::WRITE);
		gen::Buffer<gfnt_glyph> gfnt_glyphs;
		gen::Buffer<gfnt_kern> gfnt_kerns;
		gfnt_header header;
		header.range = (float)msdf_range;
		header.margin = bitmap_margin;

		gen::Buffer<uint16_t> glyph_id_mapping(UINT16_MAX + 1);

		for (auto& range : ranges) {
			for (size_t c = range.start; c <= range.end; c++) {
				uint16_t ft_index = FT_Get_Char_Index(face, c);
				if(ft_index != 0) {
					glyph_id_mapping[header.glyph_count] = ft_index;
					header.glyph_count++;

					FT_Load_Glyph(face, ft_index, FT_LOAD_NO_BITMAP);
					auto metrics = face->glyph->metrics;

					gfnt_glyph* glyph = gfnt_glyphs.add({});
					glyph->codepoint = c;
					glyph->bearingX = metrics.horiBearingX;
					glyph->bearingY = metrics.horiBearingY;
					glyph->advance = metrics.horiAdvance;
					glyph->width = metrics.width;
					glyph->height = metrics.height;
				}
			}
		}

		if (FT_HAS_KERNING(face)) {
			for (size_t i = 0; i < header.glyph_count; i++) {
				uint16_t left = glyph_id_mapping[i];
				for (size_t j = 0; j < header.glyph_count; j++) {
					uint16_t right = glyph_id_mapping[j];
					FT_Vector vec;
					if (FT_Get_Kerning(face, left, right, FT_KERNING_UNFITTED, &vec) == 0) {
						if ((vec.x != 0) || (vec.y != 0)) {
							header.kerning_pairs++;
							gfnt_kern* k = gfnt_kerns.add({});
							k->left = i;
							k->right = j;
							k->x = vec.x;
							k->y = vec.y;
						}
					}
				}
			}
		}

		gen::ByteBuffer data;
		gen::ByteBuffer glyph_sdf_data;
		gen::ByteBuffer data_zlib;

		msdfgen::FontHandle* msdfgen_ft = msdfgen::adoptFreetypeFont(face);
		msdfgen::Shape shape;

		for (size_t i = 0; i < header.glyph_count; i++) {
			uint16_t glyph = glyph_id_mapping[i];
			shape.contours.clear();
			if(msdfgen::loadGlyph(shape, msdfgen_ft, msdfgen::GlyphIndex(glyph), msdfgen::FONT_SCALING_EM_NORMALIZED)) {
				if(!shape.contours.empty()) {
					shape.normalize();
					shape.inverseYAxis = true;

					auto bounds = shape.getBounds();
					double ww = fabs(bounds.r - bounds.l);
					double hh = fabs(bounds.b - bounds.t);

					msdfgen::Projection projection1(msdfgen::Vector2(1.0 / ww, 1.0 / hh), msdfgen::Vector2(-bounds.l, -bounds.b));
					for(auto &c : shape.contours) {
						for(auto& e : c.edges) {
							auto* p = e->controlPoints();
							size_t p_count = e->type() + 1;
							for(size_t i = 0; i < p_count; i++) {
								*((msdfgen::Vector2*)p) = projection1.project(*p);
								p++;
							}
						}
					}
					uint32_t bitmap_w = ceil(ww * bitmap_size);
					uint32_t bitmap_h = ceil(hh * bitmap_size);

					msdfgen::Vector2 msdf_scale(bitmap_w, bitmap_h);
					msdfgen::Vector2 msdf_pos((float)bitmap_margin/bitmap_w, (float)bitmap_margin/bitmap_h);
					bitmap_w += (bitmap_margin * 2);
					bitmap_h += (bitmap_margin * 2);

					msdfgen::edgeColoringSimple(shape, 3.0);
					msdfgen::Bitmap<float, 4> bitmap(bitmap_w, bitmap_h);
					msdfgen::SDFTransformation t(msdfgen::Projection(msdf_scale, msdf_pos), msdfgen::Range(0.125));
					msdfgen::generateMTSDF(bitmap, shape, t);

					gen::ByteBuffer msdf_data;
					msdf_data.reserve(bitmap_w * bitmap_h * 4);
					gfnt_glyphs[i].offset = glyph_sdf_data.size_bytes();
					gfnt_glyphs[i].size = msdf_data.size_bytes();
					gfnt_glyphs[i].bitmapW = bitmap_w;
					gfnt_glyphs[i].bitmapH = bitmap_h;

					float* in = bitmap;
					auto* out = (uint8_t*)msdf_data.ptr();
					for(size_t x = 0; x < bitmap_w; x++) {
						for(int32_t y = 0; y < bitmap_h; y++) {
							out[(x + (y * bitmap_w)) * 4 + 0] = msdfgen::pixelFloatToByte(bitmap(x, y)[0]);
							out[(x + (y * bitmap_w)) * 4 + 1] = msdfgen::pixelFloatToByte(bitmap(x, y)[1]);
							out[(x + (y * bitmap_w)) * 4 + 2] = msdfgen::pixelFloatToByte(bitmap(x, y)[2]);
							out[(x + (y * bitmap_w)) * 4 + 3] = msdfgen::pixelFloatToByte(bitmap(x, y)[3]);
						}
					}
					glyph_sdf_data.add(msdf_data.ptr(), msdf_data.size_bytes());
					stbi_write_png(gen::String::createf("img-out/%zu.png", i).c_str(), bitmap_w, bitmap_h, 4, msdf_data.ptr(), bitmap_w * 4);
					printf("Glyph: %zu/%zu\n", i, (size_t)header.glyph_count);
				}
			}
		}
		msdfgen::destroyFont(msdfgen_ft);
		FT_Done_Face(face);
		FT_Done_FreeType(ft_lib);

		data.add(gfnt_glyphs.ptr(), gfnt_glyphs.size_bytes());
		data.add(gfnt_kerns.ptr(), gfnt_kerns.size_bytes());
		data.add(glyph_sdf_data.ptr(), glyph_sdf_data.size_bytes());

		gen::compressor_zlib()->compress(data_zlib, data.ptr(), data.size_bytes());
		header.uncompressed_size = data.size();
		header.compressed_size = data_zlib.size();

		out_file.write(&header, sizeof(gfnt_header));
		out_file.write(data_zlib.ptr(), data_zlib.size_bytes());
		out_file.close();

		printf("Saved %s\n", output.c_str());
	}
}
